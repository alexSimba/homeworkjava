package com.company;

public class Logger {
    private static final Logger logger;

    static {
        logger = new Logger();
    }

    public void log(String message) {
    System.out.println(message);
    }

    public static Logger getLogger() {
        return logger;
    }
}
