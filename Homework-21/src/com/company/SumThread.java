package com.company;

/**
 * 20.11.2021
 * 22. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class SumThread extends Thread {
    private int from;
    private int to;
    private int threadNumber;

    public SumThread(int threadNumber, int from, int to) {
        this.threadNumber = threadNumber;
        this.from = from;
        this.to = to;

    }
    @Override
    public void run() {
        for (int num = 0; num < Main.array.length; num++) {
                if (from <= num && to > num) {
                    Main.sums[threadNumber] += Main.array[num];
                }
        }
    }

    public int getFrom() {
        return from;
    }


    public void run(Runnable runnable) {
        runnable.run();
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }
}
