package com.company;

import java.util.Random;
import java.util.Scanner;

/**
 * 20.11.2021
 * 22. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {

    public static int array[];

    public static int sums[];

    public static int sum;



    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        int numbersCount = scanner.nextInt();
        int threadsCount = scanner.nextInt();

        array = new int[numbersCount];
        sums = new int[threadsCount];

        // заполняем случайными числами
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }

        int realSum = 0;

        for (int i = 0; i < array.length; i++) {
            realSum += array[i];
        }

        // для 2000000 -> 98996497, 98913187
        System.out.println(realSum);
        SumThread[] arrayThread = new SumThread[threadsCount];


        for (int i = 0; i < threadsCount; i++) {
            arrayThread[i] = new SumThread(i,i * numbersCount / threadsCount, (i + 1) * numbersCount / threadsCount);
        }


        for (int numRun = 0; numRun < threadsCount; numRun++) {
            arrayThread[numRun].start();
        }
        try {
            for (int numRun = 0; numRun < threadsCount; numRun++) {
                arrayThread[numRun].join();
            }
        } catch (InterruptedException e) {
            throw new IllegalArgumentException(e);
        }

        int byThreadSum = 0;

        for (int i = 0; i < threadsCount; i++) {
            byThreadSum += sums[i];
        }

        System.out.println(byThreadSum);
    }
}
