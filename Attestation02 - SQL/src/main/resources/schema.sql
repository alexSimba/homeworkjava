create table products
(
    id serial primary key,
    product_name varchar,
    prise decimal,
    number_of_product integer
);

create table customers
(
    id serial primary key,
    customers_name_and_surname varchar
);

create table orders
(
    id_product integer,
    foreign key (id_product) references products(id),
    id_customer integer,
    foreign key (id_customer) references customers(id),
    date date,
    number_of_product integer
);
insert into products(product_name, prise, number_of_product) values ('water', 40.2, 5);
insert into products(product_name, prise, number_of_product) values ('oil', 200.3, '1');
insert into products(product_name, prise, number_of_product) values ('zewa_delux', 100, 10);
insert into products(product_name, prise, number_of_product) values ('chocolate', 77.66, 100);
insert into products(product_name, prise, number_of_product) values ('vodka', 300, 10);

insert into customers(customers_name_and_surname) values ('alex_vitkalov');
insert into customers(customers_name_and_surname) values ('мarseilles_sidikov');
insert into customers(customers_name_and_surname) values ('alex_ivanov');


insert into orders(id_product, id_customer, date, number_of_product) values (3,1,'2001-09-28',3);
insert into orders(id_product, id_customer, date, number_of_product) values (4,2,'2021-08-17',50);
insert into orders(id_product, id_customer, date, number_of_product) values (1,3,'2020-01-01',5);
insert into orders(id_product, id_customer, date, number_of_product) values (4,1,'2019-08-17',50);
--вывести продукты и его цену у которого цена меньше 100, и он есть в наличие по убыванию цены
select product_name,prise
from products where prise < 100 and number_of_product > 0
order by prise DESC ;

--вывести имена покупателей, который покупали в 21 году
select customers_name_and_surname from customers
where customers.id in
      (select id_customer from orders where date >= '2021-01-01' and date < '2022-01-01');

-- обновить количество шоколадок после заказов
update products
SET number_of_product = number_of_product - (select SUM(number_of_product) from orders where id_product = 4)
where id =4
