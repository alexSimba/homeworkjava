package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите длину массива: ");
        int num = in.nextInt();
        int[] mas = new int[num];
        for (int i = 0; i < mas.length; i++) {
            System.out.printf("Введите значение элемента массива %d : ", i);
            int x = in.nextInt();
            mas[i] = x;
        }
        System.out.print("Введите значение числа для поиска в массиве: ");
        num = in.nextInt();
        int x = getIndex(mas, num);
        System.out.printf("Данное значение находиться на %d месте в массиве. ", x);
        masProcessing(mas);
        System.out.println("После обработки массив имеет вид: ");
        for (int i : mas) {
            System.out.println(i);
        }
    }

    public static int getIndex(int[] mas, int num) {
        for (int i = 0; i < mas.length; i++) {
            if (num == mas[i]) {
                return i;
            }
        }
        return -1;
    }

    public static void masProcessing(int[] mas) {
        for (int i = 0; i < mas.length; i++) {
            if (0 == mas[i]) {
                for (int j = i + 1; j < mas.length; j++) {
                    mas[i] = mas[j];
                    if (mas[j] != 0) {
                        mas[j] = 0;
                        break;
                    }
                }
            }
        }

    }
}