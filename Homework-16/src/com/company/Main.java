package com.company;

public class Main {

    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(33);
        numbers.add(15);
        numbers.add(11);
        numbers.add(89);
        numbers.add(17);
        numbers.add(21);
        numbers.add(89);
        numbers.add(123);
        numbers.add(321);
        numbers.add(83);
        numbers.add(33);
        numbers.add(19);
        numbers.add(42);
        numbers.add(10);
        numbers.add(17);
        numbers.add(8);
        numbers.add(5);
        System.out.println("Длина массива до удаления: "+numbers.getSize());
        System.out.println("Массив до удаления ");
        for (int i = 0; i < numbers.getSize(); i++) {
            System.out.println(numbers.get(i));
        }
        numbers.removeAt(5);
        System.out.println("Длина массива после удаления "+numbers.getSize());
        System.out.println(" Массив после удаления ");
        for (int i = 0; i < numbers.getSize(); i++) {
            System.out.println(numbers.get(i));
        }
    }
}
