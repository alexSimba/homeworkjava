package com.company;

public class Circle extends Ellipse {
    public Circle(int x) {
        super(x, x);
    }

    public double getPerimeter() {

        return 2 * 3.14 * x;

    }
}
