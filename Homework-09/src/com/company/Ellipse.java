package com.company;


public class Ellipse extends Figure {

    public Ellipse(int x, int y) {
        super(x, y);
    }

    public double getPerimeter() {
        double p = 0;
        if (x > y) {
            p = 4 * ((3.14 * x * y + x - y) / (x + y));
        }
        if (x <= y) {
            p = 4 * ((3.14 * x * y - x + y) / (x + y));
        }
        return p;
    }
}
