package com.company;

public class Rectangle extends Figure{
    public Rectangle(int x, int y) {
        super(x, y);
    }

    public double getPerimeter() {

        return  4 * x + y * 4;

    }
}
