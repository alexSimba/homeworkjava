package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        People[] masPeople = new People[10];
        Scanner input;
        People people;
        for (int i = 0; i < masPeople.length; i++) {
            input = new Scanner(System.in);
            System.out.println("введите имя человека ");
            String name = input.nextLine();
            System.out.println("введите вес человека ");
            double weight = input.nextInt();
            masPeople[i] = new People(name, weight);
            input.useDelimiter("/");
            input = new Scanner("/");
        }
        for (int i = 0; i < masPeople.length; i++) {
            for (int j = i + 1; j < masPeople.length; j++) {
                people = masPeople[i];
                if (masPeople[i].getWeight() < masPeople[j].getWeight()) {
                    masPeople[i] = masPeople[j];
                    masPeople[j]= people;
                }
            }
        }
        for (People peopleForPrint : masPeople) {
            System.out.println("Имя: " + peopleForPrint.getName() + "  Вес: " + peopleForPrint.getWeight());
        }
    }
}
