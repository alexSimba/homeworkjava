package com.company;

public class People {
    private String name;
    private double weight;

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public People() {
    }

    public People(String name, double weight) {
        this.name = name;
        this.weight = weight;
    }
}
