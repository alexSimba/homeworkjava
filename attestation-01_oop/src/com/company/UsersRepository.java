package com.company;

public interface UsersRepository {
    User findById(int id);
    void update(User user);

}
