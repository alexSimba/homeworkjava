package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UsersRepositoryFileImpl implements UsersRepository {
    private String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public User findById(int id) {
        User user = new User();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            reader.lines()
                    .map(line -> line.replaceAll(" ", "").split("\\|"))
                    .filter(line -> line[0].equals(Integer.toString(id)))
                    .peek(line -> user.setId(Integer.parseInt(line[0])))
                    .peek(line -> user.setName(line[1]))
                    .peek(line -> user.setAge(Integer.parseInt(line[2])))
                    .peek(line -> user.setWorker(line[3].equals("true")))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        if (user.getName() == null) {
            return null;
        }
        return user;
    }

    @Override
    public void update(User user) {
        List<String[]> listString;
        List<User> arrayUser = new ArrayList<>();
        boolean ok = false;
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            listString = reader.lines()
                    .map(line -> line.replaceAll(" ", "").split("\\|"))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        for (String[] i : listString) {
            arrayUser.add(new User(Integer.parseInt(i[0]), i[1], Integer.parseInt(i[2]), i[3].equals("true")));
        }
        for (int i = 0; i < arrayUser.size(); i++) {
            if (arrayUser.get(i).getId() == user.getId()) {
                arrayUser.set(i, user);
                ok = true;
            }
        }
        if (ok) {
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName))) {
                for (User userForWrite : arrayUser) {
                    String text = userForWrite.getId() + "|" + userForWrite.getName() + "|" + userForWrite.getAge() + "|" + userForWrite.isWorker()+"\n";
                    bw.write(text);
                }
            } catch (IOException ex) {
                throw new IllegalArgumentException();
            }
        } else {
            System.out.println("Пользователь не найден");
        }
    }
}
