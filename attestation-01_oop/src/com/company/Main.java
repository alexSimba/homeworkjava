package com.company;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");
        User user = usersRepository.findById(1);
        user.setName("Марсель");
        user.setAge(27);
        usersRepository.update(user);
    }
}
