package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        ArrayList<Integer> inputList = new ArrayList<>();
        int minNum = 10;
        System.out.print("Введите числа для проверки. После каждого числа нажмите enter. -1 является концом ввода: ");
        for (int num = 0; num != -1; ) {
            num = input.nextInt();
            inputList.add(num);
        }
        for (int numCheckRemainder : inputList) {

            if (numCheckRemainder == 0) {
                minNum = numCheckRemainder;
                break;
            }
            if (numCheckRemainder == -1) {
                break;
            }
            numCheckRemainder = Math.abs(numCheckRemainder);
            for (int targetNum = numCheckRemainder; numCheckRemainder >= 1; numCheckRemainder = numCheckRemainder / 10) {
                targetNum = numCheckRemainder % 10;
                if (targetNum < minNum) {
                    minNum = targetNum;
                }
            }
        }
        System.out.println("ответ: " + minNum);
    }


}
