package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static int minNumber = 2147483647;
    private static final ArrayList<Integer> LIST = new ArrayList<>();

    public static void main(String[] args) {
        //создание массива даных
        int masLength = (int) (Math.random() * 100000000);
        int[] mas = new int[masLength];
        for (int i = 0; i < mas.length; i++) {
            int a = (int) (Math.random() * 201) - 100;
            mas[i] = a;
        }
        mas[mas.length - 1] = -1;
        getNumber(mas);
        for (Object num : LIST) {
            System.out.print(num);
        }
        System.out.printf(" - это число(числа) в массиве повторяеться: %d раз.", minNumber);
    }

    public static void getNumber(int[] mas) {
        Map<Integer, Integer> hashMap = new HashMap<>();
        for (int ma : mas) {
            if (!hashMap.containsKey(ma)) {
                hashMap.put(ma, 1);
            } else {
                hashMap.replace(ma, hashMap.get(ma) + 1);
            }
        }

        for (Map.Entry<Integer, Integer> i : hashMap.entrySet()) {
            if (minNumber > i.getValue()) {
                minNumber = i.getValue();
            }
        }
        for (Map.Entry<Integer, Integer> i : hashMap.entrySet()) {
            if (minNumber == i.getValue()) {
                LIST.add(i.getKey());
            }
        }

    }
}