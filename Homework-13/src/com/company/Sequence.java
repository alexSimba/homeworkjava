package com.company;

import java.util.ArrayList;

public class Sequence {
    public static int[] filter(int[] array, ByCondition condition) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i : array) {
            if (condition.isOk(i)) {
                list.add(i);
            }
        }
        array = list.stream().mapToInt(i -> i).toArray();
        return array;
    }

}
