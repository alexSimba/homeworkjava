package com.company;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ByCondition condition = number -> {
            ArrayList<Integer> list = new ArrayList();
            if (number % 2 != 0) {
                return false;
            }
            while (number > 0) {
                int oneNum = number % 10;
                number = number / 10;
                list.add(oneNum);
            }
            int sum = 0;
            for (int i : list) {
                sum += i;
            }
            if (sum % 2 != 0) {
                return false;
            }
            return true;
        };
        int masLength = (int) (Math.random() * 50);
        int[] mas = new int[masLength];
        for (int i = 0; i < mas.length; i++) {
            int a = (int) (Math.random() * 10000);
            mas[i] = a;
        }
        System.out.println("Массив до: ");
        for (int i : mas) {
            System.out.println(i);
        }
        Sequence sequence = new Sequence();
        mas = sequence.filter(mas, condition);
        System.out.println("Массив после: ");
        for (int i : mas) {
            System.out.println(i);
        }
    }
}
